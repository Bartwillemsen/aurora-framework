# Aurora Change Log

## Version 2.0.4

- Fix: Fixed nested sections.
- Fix: Fixed raw_where in query builder.
- Fix: Limited URI segments to 20 to protet against DDoS.
- Fix: Made "timestamps" method in Storm model protected instead of private.

### Upgrading from 2.0.3

- Replace **aurora** directory.

## Version 2.0.3

- Feature: Added default parameter to File::get method.
- Feature: Allow for message container to be passed to Redirect's "with_errors" method.
- Feature: Added array access to Session::get.
- Fix: Lowercase HTTP verbs may now be passed to Form::open method.
- Fix: Filter parameters are now merged correctly.
- Fix: Remove orderings before running pagination queries.
- Fix: Session flush now correctly prepares empty data.
- Fix: DB::raw now works on Storm properties.

### Upgrading from 2.0.2

- Replace **aurora** directory.

## Version 2.0.2

- Feature: Added new URL::to_action and URL::to_secure_action methods.
- Fix: Fixed a bug in the Autoloader's PSR-0 library detection.
- Fix: View composers should be cached on the first retrieval.

### Upgrading from 2.0.1

- Replace **aurora** directory.

## Version 2.0.1

- Fix bug in validator class that caused file uploads to be validated incorrectly.
- Add code example to File::upload method.
- Application URL is now auto-detected.

### Upgrading from 2.0.0

- Replace **aurora** directory.

## Version 2.0.0

- Added support for controllers.
- Added Redis support, along with cache and session drivers.
- Added cookie session driver.
- Added support for database expressions
- Added Edge templating engine
- Added view "sections".
- Added support for filter parameters.
- Added dependency injection and IoC support.
- Made authentication system more flexible.
- Added better PSR-0 library support.
- Added fingerprint hashing to cookies.
- Improved view error handling.
- Made input flashing more developer friendly.
- Added better Redirect shortcut methods.
- Added standalone Memcached class.
- Simplified exception handling.
- Added ability to ignore certain error levels.
- Directories re-structured.
- Improved overall code quality and architecture.
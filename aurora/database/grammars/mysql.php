<?php
namespace Aurora\Database\Grammars;

class MySQL extends Grammar
{
	/**
	 * Get the keyword identifier for the database system.
	 *
	 * @var string
	 */
	protected $wrapper = '`';
}
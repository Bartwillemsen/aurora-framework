<?php namespace Aurora;

/*
 * Define all of the constants that we will need to use the framework.
 * These are things like file extensions, as well as all of the paths
 * used by the framework. All of the paths are built on top of the
 * basic application, Aurora, and public paths.
 */
define('EXT', '.php');
define('CRLF', "\r\n");
define('EDGE_EXT', '.edge.php');
define('APP_PATH', realpath($application).'/');
define('BASE_PATH', realpath("$aurora/..").'/');
define('PUBLIC_PATH', realpath($public).'/');
define('SYS_PATH', realpath($aurora).'/');
define('STORAGE_PATH', APP_PATH.'storage/');
define('CACHE_PATH', STORAGE_PATH.'cache/');
define('CONFIG_PATH', APP_PATH.'config/');
define('CONTROLLER_PATH', APP_PATH.'controllers/');
define('DATABASE_PATH', STORAGE_PATH.'database/');
define('LANG_PATH', APP_PATH.'language/');
define('LIBRARY_PATH', APP_PATH.'libraries/');
define('MODEL_PATH', APP_PATH.'models/');
define('ROUTE_PATH', APP_PATH.'routes/');
define('SESSION_PATH', STORAGE_PATH.'sessions/');
define('SYS_CONFIG_PATH', SYS_PATH.'config/');
define('VIEW_PATH', APP_PATH.'views/');

/*
 * Define the Aurora environment configuration path. This path is used
 * by the configuration class to load configuration options specific for
 * the server environment, allowing the developer to conveniently change
 * configuration options based on the application environment.
 */
$environment = '';

if (isset($_SERVER['AURORA_ENV'])) {
	$environment = CONFIG_PATH.$_SERVER['AURORA_ENV'].'/';
}

define('ENV_CONFIG_PATH', $environment);

unset($application, $public, $aurora, $environment);

/*
 * Require all of the classes that can't be loaded by the auto-loader.
 * These are typically classes that the auto-loader itself relies upon
 * to load classes, such as the array and configuration classes.
 */
require SYS_PATH.'arr'.EXT;
require SYS_PATH.'config'.EXT;
require SYS_PATH.'facades'.EXT;
require SYS_PATH.'autoloader'.EXT;

/*
 * Load a few of the core configuration files that are loaded for every
 * request to the application. It is quicker to load them manually each
 * request rather than parse the keys for every request.
 */
Config::load('application');
Config::load('session');
Config::load('error');

/*
 * Register the Autoloader's "load" method on the auto-loader stack.
 * This method provides the lazy-loading of all class files, as well
 * as any PSR-0 compliant libraries used by the application.
 */
spl_autoload_register(array('Aurora\\Autoloader', 'load'));

/*
 * Build the Aurora framework class map. This provides a super fast
 * way of resolving any Aurora class name to its appropriate path.
 * More mappings can also be registered by the developer as needed.
 */
Autoloader::$mappings = array(
	'Aurora\\Arr' => SYS_PATH.'arr'.EXT,
	'Aurora\\Asset' => SYS_PATH.'asset'.EXT,
	'Aurora\\Auth' => SYS_PATH.'auth'.EXT,
	'Aurora\\Benchmark' => SYS_PATH.'benchmark'.EXT,
	'Aurora\\Edge' => SYS_PATH.'edge'.EXT,
	'Aurora\\Config' => SYS_PATH.'config'.EXT,
	'Aurora\\Cookie' => SYS_PATH.'cookie'.EXT,
	'Aurora\\Crypter' => SYS_PATH.'crypter'.EXT,
	'Aurora\\File' => SYS_PATH.'file'.EXT,
	'Aurora\\Form' => SYS_PATH.'form'.EXT,
	'Aurora\\Hash' => SYS_PATH.'hash'.EXT,
	'Aurora\\HTML' => SYS_PATH.'html'.EXT,
	'Aurora\\Inflector' => SYS_PATH.'inflector'.EXT,
	'Aurora\\Input' => SYS_PATH.'input'.EXT,
	'Aurora\\IoC' => SYS_PATH.'ioc'.EXT,
	'Aurora\\Lang' => SYS_PATH.'lang'.EXT,
	'Aurora\\Memcached' => SYS_PATH.'memcached'.EXT,
	'Aurora\\Messages' => SYS_PATH.'messages'.EXT,
	'Aurora\\Paginator' => SYS_PATH.'paginator'.EXT,
	'Aurora\\Redirect' => SYS_PATH.'redirect'.EXT,
	'Aurora\\Redis' => SYS_PATH.'redis'.EXT,
	'Aurora\\Request' => SYS_PATH.'request'.EXT,
	'Aurora\\Response' => SYS_PATH.'response'.EXT,
	'Aurora\\Section' => SYS_PATH.'section'.EXT,
	'Aurora\\Str' => SYS_PATH.'str'.EXT,
	'Aurora\\URI' => SYS_PATH.'uri'.EXT,
	'Aurora\\URL' => SYS_PATH.'url'.EXT,
	'Aurora\\Validator' => SYS_PATH.'validator'.EXT,
	'Aurora\\View' => SYS_PATH.'view'.EXT,
	'Aurora\\Cache\\Manager' => SYS_PATH.'cache/manager'.EXT,
	'Aurora\\Cache\\Drivers\\APC' => SYS_PATH.'cache/drivers/apc'.EXT,
	'Aurora\\Cache\\Drivers\\Driver' => SYS_PATH.'cache/drivers/driver'.EXT,
	'Aurora\\Cache\\Drivers\\File' => SYS_PATH.'cache/drivers/file'.EXT,
	'Aurora\\Cache\\Drivers\\Memcached' => SYS_PATH.'cache/drivers/memcached'.EXT,
	'Aurora\\Cache\\Drivers\\Redis' => SYS_PATH.'cache/drivers/redis'.EXT,
	'Aurora\\Database\\Connection' => SYS_PATH.'database/connection'.EXT,
	'Aurora\\Database\\Expression' => SYS_PATH.'database/expression'.EXT,
	'Aurora\\Database\\Manager' => SYS_PATH.'database/manager'.EXT,
	'Aurora\\Database\\Query' => SYS_PATH.'database/query'.EXT,
	'Aurora\\Database\\Connectors\\Connector' => SYS_PATH.'database/connectors/connector'.EXT,
	'Aurora\\Database\\Connectors\\MySQL' => SYS_PATH.'database/connectors/mysql'.EXT,
	'Aurora\\Database\\Connectors\\Postgres' => SYS_PATH.'database/connectors/postgres'.EXT,
	'Aurora\\Database\\Connectors\\SQLite' => SYS_PATH.'database/connectors/sqlite'.EXT,
	'Aurora\\Database\\Storm\\Hydrator' => SYS_PATH.'database/storm/hydrator'.EXT,
	'Aurora\\Database\\Storm\\Model' => SYS_PATH.'database/storm/models'.EXT,
	'Aurora\\Database\\Grammars\\Grammar' => SYS_PATH.'database/grammars/grammar'.EXT,
	'Aurora\\Database\\Grammars\\MySQL' => SYS_PATH.'database/grammars/mysql'.EXT,
	'Aurora\\Routing\\Controller' => SYS_PATH.'routing/controller'.EXT,
	'Aurora\\Routing\\Filter' => SYS_PATH.'routing/filter'.EXT,
	'Aurora\\Routing\\Loader' => SYS_PATH.'routing/loader'.EXT,
	'Aurora\\Routing\\Route' => SYS_PATH.'routing/route'.EXT,
	'Aurora\\Routing\\Router' => SYS_PATH.'routing/router'.EXT,
	'Aurora\\Session\\Payload' => SYS_PATH.'session/payload'.EXT,
	'Aurora\\Session\\Drivers\\APC' => SYS_PATH.'session/drivers/apc'.EXT,
	'Aurora\\Session\\Drivers\\Cookie' => SYS_PATH.'session/drivers/cookie'.EXT,
	'Aurora\\Session\\Drivers\\Database' => SYS_PATH.'session/drivers/database'.EXT,
	'Aurora\\Session\\Drivers\\Driver' => SYS_PATH.'session/drivers/driver'.EXT,
	'Aurora\\Session\\Drivers\\Factory' => SYS_PATH.'session/drivers/factory'.EXT,
	'Aurora\\Session\\Drivers\\File' => SYS_PATH.'session/drivers/file'.EXT,
	'Aurora\\Session\\Drivers\\Memcached' => SYS_PATH.'session/drivers/memcached'.EXT,
	'Aurora\\Session\\Drivers\\Redis' => SYS_PATH.'session/drivers/redis'.EXT,
	'Aurora\\Session\\Drivers\\Sweeper' => SYS_PATH.'session/drivers/sweeper'.EXT,
);

/*
 * Register the default timezone for the application. This will be
 * the default timezone used by all date / timezone functions in
 * the entire application.
 */
date_default_timezone_set(Config::$items['application']['timezone']);

/*
 * Define a few global, convenient functions. These functions
 * provide short-cuts for things like the retrieval of language
 * lines and HTML::entities. They just make our lives as devs a
 * little sweeter and more enjoyable.
 */
require SYS_PATH.'helpers'.EXT;
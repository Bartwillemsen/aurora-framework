<?php
namespace Aurora\Session\Drivers;

use Aurora\Crypter;

class Cookie implements Driver
{
	/**
	 * The name of the cookie used to store the session payload.
	 *
	 * @var string
	 */
	const payload = 'session_payload';

	/**
	 * Load a session from storage by a given ID.
	 *
	 * If no session is found for the ID, null will be returned.
	 *
	 * @param  string  $id
	 * @return array
	 */
	public function load($id)
	{
		if (\Aurora\Cookie::has(Cookie::payload)) {
			$cookie = Crypter::decrypt(\Aurora\Cookie::get('session_payload'));

			return unserialize($cookie);
		}
	}

	/**
	 * Save a given session to storage.
	 *
	 * @param  array  $session
	 * @param  array  $config
	 * @param  bool   $exists
	 */
	public function save($session, $config, $exists)
	{
		extract($config, EXTR_SKIP);

		$payload = Crypter::encrypt(serialize($session));

		\Aurora\Cookie::put(Cookie::payload, $payload, $lifetime, $path, $domain);
	}

	/**
	 * Delete a session from storage by a given ID.
	 *
	 * @param  string  $id
	 */
	public function delete($id)
	{
		\Aurora\Cookie::forget(Cookie::payload);
	}
}
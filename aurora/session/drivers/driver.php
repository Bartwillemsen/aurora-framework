<?php
namespace Aurora\Session\Drivers;

interface Driver
{
	/**
	 * Load a session from storage by a given ID.
	 *
	 * If no session is found for the ID, null will be returned.
	 *
	 * @param  string  $id
	 * @return array
	 */
	public function load($id);

	/**
	 * Save a given session to storage.
	 *
	 * @param  array  $session
	 * @param  array  $config
	 * @param  bool   $exists
	 */
	public function save($session, $config, $exists);

	/**
	 * Delete a session from storage by a given ID.
	 *
	 * @param  string  $id
	 */
	public function delete($id);
} 
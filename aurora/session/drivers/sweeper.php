<?php
namespace Aurora\Session\Drivers;

interface Sweeper
{
	/**
	 * Delete all expired sessions from persistant storage.
	 *
	 * @param int  $expiration
	 */
	public function sweep($expiration);
}
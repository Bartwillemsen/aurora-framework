<?php

return array(

	/*
	 * Application URL
	 *--------------------------------------------------------------------------
	 *
	 * The URL used to access your application. No trailing slash.
	 */

	'url' => '',

	/*
	 * Application Index
	 *--------------------------------------------------------------------------
	 *
	 * If you are including the "index.php" in your URLs, you can ignore this.
	 *
	 * However, if you are using mod_rewrite or something similar to get
	 * cleaner URLs, set this option to an empty string.
	 */

	'index' => 'index.php',

	/*
	 * Application Key
	 *--------------------------------------------------------------------------
	 *
	 * The application key should be a random, 32 character string.
	 *
	 * This key is used by the encryption and cookie classes to generate secure
	 * encrypted strings and hashes. It is extremely important that this key
	 * remain secret and should not be shared with anyone.
	 */

	'key' => '',

	/*
	 * Application Character Encoding
	 *--------------------------------------------------------------------------
	 *
	 * The default character encoding used by your application. This encoding
	 * will be used by the Str, Text, and Form classes.
	 */

	'encoding' => 'UTF-8',

	/*
	 * Application Language
	 *--------------------------------------------------------------------------
	 *
	 * The default language of your application. This language will be used by
	 * Lang library as the default language when doing string localization.
	 */

	'language' => 'en',

	/*
	 * SSL Link Generation
	 *--------------------------------------------------------------------------
	 *
	 * Many sites use SSL to protect their users data. However, you may not
	 * always be able to use SSL on your development machine, meaning all HTTPS
	 * will be broken during development.
	 *
	 * For this reason, you may wish to disable the generation of HTTPS links
	 * throughout your application. This option does just that. All attempts to
	 * generate HTTPS links will generate regular HTTP links instead.
	 */

	'ssl' => true,

	/*
	 * Application Timezone
	 *--------------------------------------------------------------------------
	 *
	 * The default timezone of your application. This timezone will be used when
	 * Aurora needs a date, such as when writing to a log file.
	 */

	'timezone' => 'Europe/Amsterdam',

	/*
	 * Class Aliases
	 *--------------------------------------------------------------------------
	 *
	 * Here, you can specify any class aliases that you would like registered
	 * when Aurora loads. Aliases are lazy-loaded, so add as many as you want.
	 *
	 * Aliases make it more convenient to use namespaced classes. Instead of
	 * referring to the class using its full namespace, you may simply use
	 * the alias defined here.
	 *
	 * We have already aliased common Aurora classes to make your life easier.
	 */
	'aliases' => array(
		'Arr'        => 'Aurora\\Arr',
		'Asset'      => 'Aurora\\Asset',
		'Auth'       => 'Aurora\\Auth',
		'Autoloader' => 'Aurora\\Autoloader',
		'Benchmark'  => 'Aurora\\Benchmark',
		'Cache'      => 'Aurora\\Cache\\Manager',
		'Config'     => 'Aurora\\Config',
		'Controller' => 'Aurora\\Routing\\Controller',
		'Cookie'     => 'Aurora\\Cookie',
		'Crypter'    => 'Aurora\\Crypter',
		'DB'         => 'Aurora\\Database\\Manager',
		'File'       => 'Aurora\\File',
		'Form'       => 'Aurora\\Form',
		'Has'        => 'Aurora\\Hash',
		'HTML'       => 'Aurora\\HTML',
		'Inflector'  => 'Aurora\\Inflector',
		'Input'      => 'Aurora\\Input',
		'IoC'        => 'Aurora\\IoC',
		'Lang'       => 'Aurora\\Lang',
		'Memcached'  => 'Aurora\\Memcached',
		'Paginator'  => 'Aurora\\Paginator',
		'URL'        => 'Aurora\\URL',
		'Redirect'   => 'Aurora\\Redirect',
		'Redis'      => 'Aurora\\Redis',
		'Request'    => 'Aurora\\Request',
		'Response'   => 'Aurora\\Response',
		'Section'    => 'Aurora\\Section',
		'Session'    => 'Aurora\\Facades\\Session',
		'Storm'      => 'Aurora\\Database\\Storm\\Model',
		'Str'        => 'Aurora\\Str',
		'Validator'  => 'Aurora\\Validator',
		'View'       => 'Aurora\\View',
	),
);
<?php
/**
 * Aurora - A minimal framework for PHP development
 *
 * @package  Aurora
 * @version  2.0.4
 * @author   Bart Willemsen <b.willemsen8@gmail.com>
 * @link     https://gitlab.com/Bartwillemsen/aurora-framework
 */

/*
 * Tick... Tock... Tick... Tock...
 * --------------------------------------------------------------
 */
define('AURORA_START', microtime(true));

/*
 * The path to the application directory.
 * --------------------------------------------------------------
 */
$application = '../application';

/*
 * The path to the Aurora directory
 * --------------------------------------------------------------
 */
$aurora = '../aurora';

/*
 * The path to the public directory
 * --------------------------------------------------------------
 */
$public = __DIR__;

/*
 * Launch Aurora.
 * --------------------------------------------------------------
 */
require $aurora.'/aurora.php';
## Aurora - A minimal framework for PHP development

Aurora is a small and clean framework for PHP. It started out as a
framework to support the development of a private project, but since it was set
up in a generic way, I believed it could be used for a wide range of other
applications as well. That's why I decided to release this framework as an
open source project.

This project borrows a lot of functionality from existing frameworks and libraries
out there and combines the features we needed in a more streamlined and easy to
use way.

### [Official Website & Documentation](https://gitlab.com/Bartwillemsen/aurora-framework/wikis/home)
